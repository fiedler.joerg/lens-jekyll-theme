---
title: 2020/02/21 | Ab dem 01. Januar 2020 sind alle privaten Bahnfahrten in Uniform
  für die Soldatinnen und Soldaten kostenfrei.
caption: 今年から軍服の兵士さんは電車が無料なので兵士さんをよく見かけた。
date: 2020-02-20 23:00:00 +0000
image: "/v1582376553/684983F3-4213-480C-8266-E1990DDECEF2_mpal9j.jpg"

---
