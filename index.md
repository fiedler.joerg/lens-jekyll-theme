---
layout: home
title: In Berlin

---
<!-- Thumbnail -->
{% assign photos = site.photos | reverse %}
<section id="thumbnails">{% for photo in photos limit: 100 %}
	<article>
		<a class="thumbnail" href="{{ site.cloudinary_url }}w_1024,c_fit{{ photo.image }}" data-position="left center"><img src="{{ site.cloudinary_url }}w_250,h_250,c_thumb{{ photo.image }}" alt="" /></a>
		<h2>{{ photo.title }}</h2>
		<p>{{ photo.caption }}</p>
	</article>
{% endfor %}</section>